#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

#define INITIAL_SIZE 2048
#define DEFAULT_SIZE 256
#define RANGE_VALUE 64

/* Обычное успешное выделение памяти */
static void test_usual_success_malloc() {
    heap_init(INITIAL_SIZE);

    void *init = _malloc(DEFAULT_SIZE);
    struct block_header* block = init;

    assert(!region_is_invalid(init));
    assert(!block->is_free);

    _free(init);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

/* Освобождение одного блока из нескольких выделенных */
static void test_free_single_block() {
    heap_init(INITIAL_SIZE);

    struct block_header* first = _malloc(DEFAULT_SIZE);
    struct block_header* second = _malloc(DEFAULT_SIZE + RANGE_VALUE);
    struct block_header* third = _malloc(DEFAULT_SIZE - RANGE_VALUE);

    _free(second);
    assert(second->is_free);
    assert(!first->is_free);
    assert(!third->is_free);
    debug_heap(stderr, HEAP_START);

    _free(first);
    _free(third);

    heap_term();
}

/* Освобождение двух блоков из нескольких выделенных */
static void test_free_double_block() {
    heap_init(INITIAL_SIZE);

    struct block_header* first = _malloc(DEFAULT_SIZE);
    struct block_header* second = _malloc(DEFAULT_SIZE + RANGE_VALUE);
    struct block_header* third = _malloc(DEFAULT_SIZE - RANGE_VALUE);

    _free(first);
    _free(second);
    assert(first->is_free);
    assert(second->is_free);
    assert(!third->is_free);
    debug_heap(stderr, HEAP_START);

    _free(third);

    heap_term();
}

/* Память закончилась, новый регион памяти расширяет старый */
static void test_alloc_new_fixed_region() {
    heap_init(REGION_MIN_SIZE);

    void* first = _malloc(REGION_MIN_SIZE);
    void* second = _malloc(DEFAULT_SIZE);
    assert(second == HEAP_START + REGION_MIN_SIZE);

    _free(first);
    _free(second);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

/* Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте */
static void test_alloc_new_region() {
    heap_init(REGION_MIN_SIZE);

    void* fixed = map_pages(HEAP_START + REGION_MIN_SIZE, DEFAULT_SIZE, MAP_FIXED);
    struct block_header* init = _malloc(DEFAULT_SIZE);

    assert(init != fixed);

    munmap(fixed, DEFAULT_SIZE);
    _free(init);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

static void print_test_template(char *description, void test()) {
    printf("Testing %s:\n", description);
    test();
    printf("OK\n");
}

void run_tests() {
    print_test_template("usual allocation", test_usual_success_malloc);
    print_test_template("single block freeing", test_free_single_block);
    print_test_template("double block freeing", test_free_double_block);
    print_test_template("growing new region", test_alloc_new_fixed_region);
    print_test_template("growing new region in occupied memory", test_alloc_new_region);
    printf("All tests passed!");
}
